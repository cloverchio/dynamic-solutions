# Uses python3
def edit_distance(s, t, x=None):
    #write your code here
    if x is None:
        x = {}
    if len(s) == 0:
        return len(t)
    if len(t) == 0:
        return len(s)
    if(len(s), len(t)) in x:
        return x[(len(s), len(t))]

    deletion = 1 if s[-1] != t[-1] else 0
    match = edit_distance(s[:-1], t[:-1], x) + deletion
    mismatch = edit_distance(s[:-1], t, x) + 1
    insertion = edit_distance(s, t[:-1], x) + 1
    ans = min(match, mismatch, insertion)
    x[(len(s), len(t))] = ans
    return ans


if __name__ == "__main__":
    print(edit_distance(input(), input()))
